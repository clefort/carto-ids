# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('documents', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='document',
            name='abstract_fr',
            field=models.TextField(help_text='brief narrative summary of the content of the resource(s)', null=True, verbose_name='abstract', blank=True),
        ),
        migrations.AddField(
            model_name='document',
            name='constraints_other_fr',
            field=models.TextField(help_text='other restrictions and legal prerequisites for accessing and using the resource or metadata', null=True, verbose_name='restrictions other', blank=True),
        ),
        migrations.AddField(
            model_name='document',
            name='data_quality_statement_fr',
            field=models.TextField(help_text="general explanation of the data producer's knowledge about the lineage of a dataset", null=True, verbose_name='data quality statement', blank=True),
        ),
        migrations.AddField(
            model_name='document',
            name='purpose_fr',
            field=models.TextField(help_text='summary of the intentions with which the resource(s) was developed', null=True, verbose_name='purpose', blank=True),
        ),
        migrations.AddField(
            model_name='document',
            name='supplemental_information_fr',
            field=models.TextField(default='No information provided', help_text='any other descriptive information about the dataset', null=True, verbose_name='supplemental information'),
        ),
        migrations.AddField(
            model_name='document',
            name='title_fr',
            field=models.CharField(help_text='name by which the cited resource is known', max_length=255, null=True, verbose_name='title'),
        ),
    ]
