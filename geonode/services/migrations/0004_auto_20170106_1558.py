# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('services', '0003_auto_20160824_0245'),
    ]

    operations = [
        migrations.AlterField(
            model_name='serviceprofilerole',
            name='role',
            field=models.CharField(help_text='function performed by the responsible party', max_length=255, choices=[(b'author', "\xe9quipe qui est l'auteur de la ressource"), (b'processor', "equipe qui a trait\xe9 les donn\xe9es d'une mani\xe8re telle que la ressource a \xe9t\xe9 modifi\xe9"), (b'publisher', '\xe9quipe qui a publi\xe9 la ressource'), (b'custodian', 'groupe qui accepte la responsabilit\xe9 des donn\xe9es et assure la maintenance et le support appropri\xe9 de ces donn\xe9es.'), (b'pointOfContact', '\xe9quipe qui peut \xeatre contact\xe9e pour acqu\xe9rir ou pour obtenir des informations sur la ressource'), (b'distributor', '\xe9quipe qui distribue la ressource'), (b'user', '\xe9quipe qui utilise la ressource'), (b'resourceProvider', '\xe9quipe qui fournit la ressource'), (b'originator', '\xe9quipe qui a cr\xe9\xe9 la ressource'), (b'owner', '\xe9quipe qui poss\xe8de la ressource'), (b'principalInvestigator', "\xe9quipe cl\xe9 qui est responsable de la collecte d'informations et qui conduit la recherche")]),
        ),
    ]
