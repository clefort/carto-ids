# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('maps', '0002_auto_20170106_1558'),
    ]

    operations = [
        migrations.AlterField(
            model_name='map',
            name='abstract_en',
            field=models.TextField(default=b'', help_text='brief narrative summary of the content of the resource(s)', null=True, verbose_name='abstract', blank=True),
        ),
        migrations.AlterField(
            model_name='map',
            name='abstract_fr',
            field=models.TextField(default=b'', help_text='brief narrative summary of the content of the resource(s)', null=True, verbose_name='abstract', blank=True),
        ),
        migrations.AlterField(
            model_name='map',
            name='supplemental_information_en',
            field=models.TextField(default=b'', help_text='any other descriptive information about the dataset', null=True, verbose_name='supplemental information', blank=True),
        ),
        migrations.AlterField(
            model_name='map',
            name='supplemental_information_fr',
            field=models.TextField(default=b'', help_text='any other descriptive information about the dataset', null=True, verbose_name='supplemental information', blank=True),
        ),
    ]
