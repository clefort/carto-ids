# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('layers', '0003_auto_20160821_1919'),
    ]

    operations = [
        migrations.AddField(
            model_name='layer',
            name='abstract_fr',
            field=models.TextField(help_text='brief narrative summary of the content of the resource(s)', null=True, verbose_name='abstract', blank=True),
        ),
        migrations.AddField(
            model_name='layer',
            name='constraints_other_fr',
            field=models.TextField(help_text='other restrictions and legal prerequisites for accessing and using the resource or metadata', null=True, verbose_name='restrictions other', blank=True),
        ),
        migrations.AddField(
            model_name='layer',
            name='data_quality_statement_fr',
            field=models.TextField(help_text="general explanation of the data producer's knowledge about the lineage of a dataset", null=True, verbose_name='data quality statement', blank=True),
        ),
        migrations.AddField(
            model_name='layer',
            name='purpose_fr',
            field=models.TextField(help_text='summary of the intentions with which the resource(s) was developed', null=True, verbose_name='purpose', blank=True),
        ),
        migrations.AddField(
            model_name='layer',
            name='supplemental_information_fr',
            field=models.TextField(default='No information provided', help_text='any other descriptive information about the dataset', null=True, verbose_name='supplemental information'),
        ),
        migrations.AddField(
            model_name='layer',
            name='title_fr',
            field=models.CharField(help_text='name by which the cited resource is known', max_length=255, null=True, verbose_name='title'),
        ),
        migrations.AlterField(
            model_name='layer',
            name='elevation_regex',
            field=models.CharField(max_length=128, null=True, verbose_name='elevation regex', blank=True),
        ),
        migrations.AlterField(
            model_name='layer',
            name='has_elevation',
            field=models.BooleanField(default=False, verbose_name='has elevation'),
        ),
        migrations.AlterField(
            model_name='layer',
            name='has_time',
            field=models.BooleanField(default=False, verbose_name='has time'),
        ),
        migrations.AlterField(
            model_name='layer',
            name='is_mosaic',
            field=models.BooleanField(default=False, verbose_name='is mosaic'),
        ),
        migrations.AlterField(
            model_name='layer',
            name='time_regex',
            field=models.CharField(blank=True, max_length=128, null=True, verbose_name='time regex', choices=[(b'[0-9]{8}', 'YYYYMMDD'), (b'[0-9]{8}T[0-9]{6}', "YYYYMMDD'T'hhmmss"), (b'[0-9]{8}T[0-9]{6}Z', "YYYYMMDD'T'hhmmss'Z'")]),
        ),
    ]
