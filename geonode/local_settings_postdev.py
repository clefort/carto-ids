# -*- coding: utf-8 -*-
#########################################################################
#
# Copyright (C) 2016 OSGeo
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
#
#########################################################################

import os
import ldap
import dj_database_url

from django_auth_ldap.config import LDAPSearch, GroupOfNamesType

PROJECT_ROOT = os.path.abspath(os.path.dirname(__file__))

# If you want to enable Mosaics use the following configuration
# UPLOADER = {
##    'BACKEND': 'geonode.rest',
#    'BACKEND': 'geonode.importer',
#    'OPTIONS': {
#        'TIME_ENABLED': True,
#        'MOSAIC_ENABLED': True,
#        'GEOGIG_ENABLED': False,
#    }
# }


# Default preview library
LAYER_PREVIEW_LIBRARY = 'geoext'

AUTH_USER_MODEL = 'people.Profile'

AUTH_LDAP_SERVER_URI = "ldap://frparvm51845512.corp.capgemini.com"

AUTH_LDAP_BIND_DN = "cn=GEONODE,ou=applications,dc=aelb,dc=fr"
AUTH_LDAP_BIND_PASSWORD = "azerty"

AUTH_LDAP_USER_SEARCH = LDAPSearch("ou=personnes,dc=aelb,dc=fr", ldap.SCOPE_SUBTREE, "(cn=%(user)s)")
AUTH_LDAP_GROUP_SEARCH = LDAPSearch("ou=IDS,ou=general,ou=groupes,dc=aelb,dc=fr", ldap.SCOPE_SUBTREE,
                                    "(objectClass=groupOfNames)")
AUTH_LDAP_GROUP_TYPE = GroupOfNamesType(name_attr="cn")

AUTH_LDAP_REQUIRE_GROUP = "cn=utilisateursIDS,ou=IDS,ou=general,ou=groupes,dc=aelb,dc=fr"
AUTH_LDAP_USER_ATTR_MAP = {
    "first_name": "givenName",
    "last_name": "sn",
    "email": "mail",
    "position": "title",
    "voice": "telephoneNumber",
    "profile": "title",
    "organization": "departmentNumber"

}
AUTH_LDAP_PROFILE_ATTR_MAP = {
    #    "employee_number": "employeeNumber"
}

AUTH_LDAP_MIRROR_GROUPS = True

AUTH_LDAP_ALWAYS_UPDATE_USER = True
AUTH_LDAP_FIND_GROUP_PERMS = True
AUTH_LDAP_CACHE_GROUPS = True
AUTH_LDAP_GROUP_CACHE_TIMEOUT = 3600
AUTH_LDAP_USER_FLAGS_BY_GROUP = {
    "is_superuser": "cn=administrateursIDS,ou=IDS,ou=general,ou=groupes,dc=aelb,dc=fr",
    "is_staff": "cn=administrateursIDS,ou=IDS,ou=general,ou=groupes,dc=aelb,dc=fr",
}

# Marche pas, mis dans settings
# INSTALLED_APPS = (
#  'django_auth_ldap'
# ) + INSTALLED_APPS


AUTHENTICATION_BACKENDS = (
    'django_auth_ldap.backend.LDAPBackend',
    # 'django.contrib.auth.backends.ModelBackend',
    # 'guardian.backends.ObjectPermissionBackend',
)

DEBUG = True

LAYER_PREVIEW_LIBRARY = 'OL3'

SITEURL = os.getenv('SITEURL',"http://127.0.0.1:8000/")
GEOSERVER_LOCATION = os.getenv('GEOSERVER_LOCATION', 'http://127.0.0.1:8080/geoserver/')
GEOSERVER_PUBLIC_LOCATION = os.getenv('GEOSERVER_PUBLIC_LOCATION', 'http://127.0.0.1:8080/geoserver/')

DATABASE_URL = os.getenv(
    'DATABASE_URL',
    'sqlite:///{path}'.format(path=os.path.join(PROJECT_ROOT, 'development.db')))

DATABASES = {
    'default': dj_database_url.parse(DATABASE_URL, conn_max_age=600)
}