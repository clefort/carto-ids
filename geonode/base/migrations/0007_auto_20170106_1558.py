# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import mptt.fields


class Migration(migrations.Migration):

    dependencies = [
        ('base', '0006_auto_20160908_0754'),
    ]

    operations = [
        migrations.CreateModel(
            name='Backup',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('identifier', models.CharField(max_length=255, editable=False)),
                ('name', models.CharField(max_length=100)),
                ('name_en', models.CharField(max_length=100, null=True)),
                ('name_fr', models.CharField(max_length=100, null=True)),
                ('date', models.DateTimeField(auto_now_add=True)),
                ('description', models.TextField(null=True, blank=True)),
                ('description_en', models.TextField(null=True, blank=True)),
                ('description_fr', models.TextField(null=True, blank=True)),
                ('base_folder', models.CharField(max_length=100)),
                ('location', models.TextField(null=True, blank=True)),
            ],
            options={
                'ordering': ('date',),
                'verbose_name_plural': 'Backups',
            },
        ),
        migrations.CreateModel(
            name='TopicDomain',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('identifier', models.CharField(default=b'', max_length=255)),
                ('description', models.TextField(default=b'', null=True)),
                ('description_en', models.TextField(default=b'', null=True)),
                ('description_fr', models.TextField(default=b'', null=True)),
                ('gn_description', models.TextField(default=b'', null=True, verbose_name=b'GeoNode description')),
                ('gn_description_en', models.TextField(default=b'', null=True, verbose_name=b'GeoNode description')),
                ('gn_description_fr', models.TextField(default=b'', null=True, verbose_name=b'GeoNode description')),
                ('is_choice', models.BooleanField(default=True)),
            ],
            options={
                'ordering': ('identifier',),
                'verbose_name_plural': 'Metadata Topic Domains',
            },
        ),
        migrations.CreateModel(
            name='Zone',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=255)),
                ('name_en', models.CharField(max_length=255, null=True)),
                ('name_fr', models.CharField(max_length=255, null=True)),
                ('lft', models.PositiveIntegerField(editable=False, db_index=True)),
                ('rght', models.PositiveIntegerField(editable=False, db_index=True)),
                ('tree_id', models.PositiveIntegerField(editable=False, db_index=True)),
                ('level', models.PositiveIntegerField(editable=False, db_index=True)),
                ('parent', mptt.fields.TreeForeignKey(related_name='children', blank=True, to='base.Zone', null=True)),
            ],
            options={
                'ordering': ('name',),
                'verbose_name_plural': 'Metadata Areas of interest',
            },
        ),
        migrations.AddField(
            model_name='license',
            name='description_fr',
            field=models.TextField(null=True, blank=True),
        ),
        migrations.AddField(
            model_name='license',
            name='license_text_fr',
            field=models.TextField(null=True, blank=True),
        ),
        migrations.AddField(
            model_name='license',
            name='name_fr',
            field=models.CharField(max_length=100, null=True),
        ),
        migrations.AddField(
            model_name='region',
            name='name_fr',
            field=models.CharField(max_length=255, null=True),
        ),
        migrations.AddField(
            model_name='restrictioncodetype',
            name='description_fr',
            field=models.TextField(max_length=255, null=True, editable=False),
        ),
        migrations.AddField(
            model_name='restrictioncodetype',
            name='gn_description_fr',
            field=models.TextField(max_length=255, null=True, verbose_name=b'GeoNode description'),
        ),
        migrations.AddField(
            model_name='spatialrepresentationtype',
            name='description_fr',
            field=models.CharField(max_length=255, null=True, editable=False),
        ),
        migrations.AddField(
            model_name='spatialrepresentationtype',
            name='gn_description_fr',
            field=models.CharField(max_length=255, null=True, verbose_name=b'GeoNode description'),
        ),
        migrations.AddField(
            model_name='topiccategory',
            name='description_fr',
            field=models.TextField(default=b'', null=True),
        ),
        migrations.AddField(
            model_name='topiccategory',
            name='gn_description_fr',
            field=models.TextField(default=b'', null=True, verbose_name=b'GeoNode description'),
        ),
        migrations.AlterField(
            model_name='contactrole',
            name='resource',
            field=models.ForeignKey(blank=True, to='base.ResourceBase', null=True),
        ),
        migrations.AlterField(
            model_name='contactrole',
            name='role',
            field=models.CharField(help_text='function performed by the responsible party', max_length=255, choices=[(b'author', "\xe9quipe qui est l'auteur de la ressource"), (b'processor', "equipe qui a trait\xe9 les donn\xe9es d'une mani\xe8re telle que la ressource a \xe9t\xe9 modifi\xe9"), (b'publisher', '\xe9quipe qui a publi\xe9 la ressource'), (b'custodian', 'groupe qui accepte la responsabilit\xe9 des donn\xe9es et assure la maintenance et le support appropri\xe9 de ces donn\xe9es.'), (b'pointOfContact', '\xe9quipe qui peut \xeatre contact\xe9e pour acqu\xe9rir ou pour obtenir des informations sur la ressource'), (b'distributor', '\xe9quipe qui distribue la ressource'), (b'user', '\xe9quipe qui utilise la ressource'), (b'resourceProvider', '\xe9quipe qui fournit la ressource'), (b'originator', '\xe9quipe qui a cr\xe9\xe9 la ressource'), (b'owner', '\xe9quipe qui poss\xe8de la ressource'), (b'principalInvestigator', "\xe9quipe cl\xe9 qui est responsable de la collecte d'informations et qui conduit la recherche")]),
        ),
        migrations.AlterField(
            model_name='link',
            name='resource',
            field=models.ForeignKey(blank=True, to='base.ResourceBase', null=True),
        ),
        migrations.AlterField(
            model_name='resourcebase',
            name='date_type',
            field=models.CharField(default='publication', help_text='identification of when a given event occurred', max_length=255, verbose_name='date type', choices=[('cr\xe9ation', 'Creation'), ('publication', 'Publication'), ('r\xe9vision', 'Revision')]),
        ),
        migrations.AlterField(
            model_name='resourcebase',
            name='language',
            field=models.CharField(default=b'eng', help_text='language used within the dataset', max_length=3, verbose_name='language', choices=[(b'abk', b'Abkhazian'), (b'aar', b'Afar'), (b'afr', b'Afrikaans'), (b'amh', b'Amharic'), (b'ara', b'Arabic'), (b'asm', b'Assamese'), (b'aym', b'Aymara'), (b'aze', b'Azerbaijani'), (b'bak', b'Bashkir'), (b'ben', b'Bengali'), (b'bih', b'Bihari'), (b'bis', b'Bislama'), (b'bre', b'Breton'), (b'bul', b'Bulgarian'), (b'bel', b'Byelorussian'), (b'cat', b'Catalan'), (b'cos', b'Corsican'), (b'dan', b'Danish'), (b'dzo', b'Dzongkha'), (b'eng', b'English'), (b'epo', b'Esperanto'), (b'est', b'Estonian'), (b'fao', b'Faroese'), (b'fij', b'Fijian'), (b'fin', b'Finnish'), (b'fra', 'Fran\xe7ais'), (b'fry', b'Frisian'), (b'glg', b'Gallegan'), (b'ger', b'German'), (b'kal', b'Greenlandic'), (b'grn', b'Guarani'), (b'guj', b'Gujarati'), (b'hau', b'Hausa'), (b'heb', b'Hebrew'), (b'hin', b'Hindi'), (b'hun', b'Hungarian'), (b'ind', b'Indonesian'), (b'ina', b'Interlingua (International Auxiliary language Association)'), (b'iku', b'Inuktitut'), (b'ipk', b'Inupiak'), (b'ita', b'Italian'), (b'jpn', b'Japanese'), (b'kan', b'Kannada'), (b'kas', b'Kashmiri'), (b'kaz', b'Kazakh'), (b'khm', b'Khmer'), (b'kin', b'Kinyarwanda'), (b'kir', b'Kirghiz'), (b'kor', b'Korean'), (b'kur', b'Kurdish'), (b'oci', b"Langue d 'Oc (post 1500)"), (b'lao', b'Lao'), (b'lat', b'Latin'), (b'lav', b'Latvian'), (b'lin', b'Lingala'), (b'lit', b'Lithuanian'), (b'mlg', b'Malagasy'), (b'mlt', b'Maltese'), (b'mar', b'Marathi'), (b'mol', b'Moldavian'), (b'mon', b'Mongolian'), (b'nau', b'Nauru'), (b'nep', b'Nepali'), (b'nor', b'Norwegian'), (b'ori', b'Oriya'), (b'orm', b'Oromo'), (b'pan', b'Panjabi'), (b'pol', b'Polish'), (b'por', b'Portuguese'), (b'pus', b'Pushto'), (b'que', b'Quechua'), (b'roh', b'Rhaeto-Romance'), (b'run', b'Rundi'), (b'rus', b'Russian'), (b'smo', b'Samoan'), (b'sag', b'Sango'), (b'san', b'Sanskrit'), (b'scr', b'Serbo-Croatian'), (b'sna', b'Shona'), (b'snd', b'Sindhi'), (b'sin', b'Singhalese'), (b'ssw', b'Siswant'), (b'slv', b'Slovenian'), (b'som', b'Somali'), (b'sot', b'Sotho'), (b'spa', b'Spanish'), (b'sun', b'Sudanese'), (b'swa', b'Swahili'), (b'tgl', b'Tagalog'), (b'tgk', b'Tajik'), (b'tam', b'Tamil'), (b'tat', b'Tatar'), (b'tel', b'Telugu'), (b'tha', b'Thai'), (b'tir', b'Tigrinya'), (b'tog', b'Tonga (Nyasa)'), (b'tso', b'Tsonga'), (b'tsn', b'Tswana'), (b'tur', b'Turkish'), (b'tuk', b'Turkmen'), (b'twi', b'Twi'), (b'uig', b'Uighur'), (b'ukr', b'Ukrainian'), (b'urd', b'Urdu'), (b'uzb', b'Uzbek'), (b'vie', b'Vietnamese'), (b'vol', b'Volap\xc3\xbck'), (b'wol', b'Wolof'), (b'xho', b'Xhosa'), (b'yid', b'Yiddish'), (b'yor', b'Yoruba'), (b'zha', b'Zhuang'), (b'zul', b'Zulu')]),
        ),
        migrations.AlterField(
            model_name='resourcebase',
            name='metadata_uploaded_preserve',
            field=models.BooleanField(default=False, verbose_name='metadata uploaded preserve'),
        ),
        migrations.AlterField(
            model_name='resourcebase',
            name='thumbnail_url',
            field=models.TextField(null=True, verbose_name='thumbnail url', blank=True),
        ),
        migrations.AddField(
            model_name='resourcebase',
            name='domains',
            field=models.ManyToManyField(help_text=None, to='base.TopicDomain', verbose_name='domains'),
        ),
        migrations.AddField(
            model_name='resourcebase',
            name='zones',
            field=models.ManyToManyField(help_text=None, to='base.Zone', verbose_name='keywords zone', blank=True),
        ),
    ]
