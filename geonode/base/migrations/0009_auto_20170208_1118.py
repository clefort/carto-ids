# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('base', '0008_auto_20170123_1736'),
    ]

    operations = [
        migrations.AlterField(
            model_name='resourcebase',
            name='category',
            field=models.ForeignKey(default=9, blank=True, to='base.TopicCategory', help_text='high-level geographic data thematic classification to assist in the grouping and search of available geographic data sets.', null=True),
        ),
    ]
