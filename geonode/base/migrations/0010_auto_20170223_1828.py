# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('base', '0009_auto_20170208_1118'),
    ]

    operations = [
        migrations.AlterField(
            model_name='resourcebase',
            name='abstract',
            field=models.TextField(default=b'', help_text='brief narrative summary of the content of the resource(s)', verbose_name='abstract', blank=True),
        ),
        migrations.AlterField(
            model_name='resourcebase',
            name='supplemental_information',
            field=models.TextField(default=b'', help_text='any other descriptive information about the dataset', null=True, verbose_name='supplemental information', blank=True),
        ),
    ]
