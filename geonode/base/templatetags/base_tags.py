# -*- coding: utf-8 -*-
#########################################################################
#
# Copyright (C) 2016 OSGeo
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
#
#########################################################################

from django import template

from agon_ratings.models import Rating
from django.contrib.contenttypes.models import ContentType
from django.contrib.auth import get_user_model
from django.db.models import Count
from django.utils.translation import ugettext_lazy as _

from guardian.shortcuts import get_objects_for_user
from geonode import settings

from geonode.layers.models import Layer
from geonode.maps.models import Map
from geonode.documents.models import Document
from geonode.groups.models import GroupProfile

register = template.Library()


@register.assignment_tag
def num_ratings(obj):
    ct = ContentType.objects.get_for_model(obj)
    return len(Rating.objects.filter(object_id=obj.pk, content_type=ct))


@register.assignment_tag(takes_context=True)
def facets(context):
    request = context['request']
    title_filter = request.GET.get('title__icontains', '')

    facet_type = context['facet_type'] if 'facet_type' in context else 'all'

    if not settings.SKIP_PERMS_FILTER:
        authorized = get_objects_for_user(
            request.user, 'base.view_resourcebase').values('id')

    if facet_type == 'documents':

        documents = Document.objects.filter(title__icontains=title_filter)

        if settings.RESOURCE_PUBLISHING:
            documents = documents.filter(is_published=True)

        if not settings.SKIP_PERMS_FILTER:
            documents = documents.filter(id__in=authorized)

        counts = documents.values('doc_type').annotate(count=Count('doc_type'))
        facets = dict([(count['doc_type'], count['count']) for count in counts])

    elif facet_type == 'layers' or facet_type == 'all':

        layers = Layer.objects.filter(title__icontains=title_filter)

        if settings.RESOURCE_PUBLISHING:
            layers = layers.filter(is_published=True)

        if not settings.SKIP_PERMS_FILTER:
            layers = layers.filter(id__in=authorized)

        counts = layers.values('storeType').annotate(count=Count('storeType'))
        count_dict = dict([(count['storeType'], count['count']) for count in counts])

        facets = [
            ['raster', _('raster'), count_dict.get('coverageStore', 0)],
            ['vector', _('vector'), count_dict.get('dataStore', 0)],
            ['remote', _('remote'), count_dict.get('remoteStore', 0)],
            ['wms', _('wms'), count_dict.get('wmsStore', 0)]
        ]

        if facet_type == 'all':
            maps = Map.objects.filter(title__icontains=title_filter)
            documents = Document.objects.filter(title__icontains=title_filter)

            if not settings.SKIP_PERMS_FILTER:
                maps = maps.filter(id__in=authorized)
                documents = documents.filter(id__in=authorized)

            facets.append(['map', _('map'), maps.count()])
            facets.append(['document', _('document'), documents.count()])

    elif facet_type == 'home':

        facets={}

        layers = Layer.objects.filter(title__icontains=title_filter)
        maps = Map.objects.filter(title__icontains=title_filter)
        documents = Document.objects.filter(title__icontains=title_filter)

        if not settings.SKIP_PERMS_FILTER:
            layers = layers.filter(id__in=authorized)
            maps = maps.filter(id__in=authorized)
            documents = documents.filter(id__in=authorized)

        facets['map'] = maps.count()
        facets['document'] = documents.count()
        facets['layer'] = layers.count()

        facets['user'] = get_user_model().objects.exclude(
                username='AnonymousUser').count()

        facets['group'] = GroupProfile.objects.exclude(
                access="private").count()
    else:
        raise Exception("Missing facet_type")

    return facets


@register.assignment_tag(takes_context=True)
def get_current_path(context):
    request = context['request']
    return request.get_full_path()


@register.assignment_tag(takes_context=True)
def get_context_resourcetype(context):
    c_path = get_current_path(context)
    resource_types = ['layers', 'maps', 'documents', 'search', 'people',
                      'groups']
    for resource_type in resource_types:
        if "/{0}/".format(resource_type) in c_path:
            return resource_type
    return 'error'
