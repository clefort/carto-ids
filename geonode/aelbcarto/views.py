# -*- coding: utf-8 -*-
#########################################################################
#
# Copyright (C) 2016 OSGeo
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
#
#########################################################################

from django.shortcuts import render
from django.http import HttpResponse
from .forms import update_geoserver_params
from django.core.management import call_command
from StringIO import StringIO
import sys
import json



def get_update_data(request):
    form = update_geoserver_params()

    return render(request, 'layer_update.html', locals())


def update_logs(request):
    if request.method == 'POST':
        post_workspace = request.POST.get('the_workspace')
        post_store = request.POST.get('the_store')
        post_ignore_known = request.POST.get('the_ignore_known')
        post_remove_deleted = request.POST.get('the_remove_deleted'),
        post_author = request.POST.get('the_author'),
        author_list = []
        for author in post_author:
            author_list.append(author)
        input_data = {'w': post_workspace, 's': post_store, 'u': author_list[0]}
        check_data = {'skip_geonode_registered': post_ignore_known, 'remove_deleted': post_remove_deleted}
        for k in input_data.keys():
            if str(input_data[k]) == '':
                if k == 'u':
                    input_data[k] = str(request.user)
                else:
                    input_data[k] = None
        for k in check_data.keys():
            if str(check_data[k]) == 'false':
                check_data[k] = False
            elif str(check_data[k] == 'true'):
                check_data[k] = True
        orig_stdout = sys.stdout
        sys.stdout = update_log = StringIO()
        call_command('updatelayers',
                        verbosity=3,
                        workspace=input_data['w'],
                        store=input_data['s'],
                        user=input_data['u'],
                        skip_geonode_registered=check_data['skip_geonode_registered'],
                        remove_deleted=check_data['remove_deleted'],
                        ignore_errors=True,
                        interactive=False,
                        stdout=update_log)
        sys.stdout = orig_stdout
        update_log.seek(0)
        response_data = update_log.read().split("\n")
        return HttpResponse(json.dumps(response_data), content_type="application/json")

    else:
        return HttpResponse(
            json.dumps({"nothing to see": "this isn't happening"}), content_type="application/json")

