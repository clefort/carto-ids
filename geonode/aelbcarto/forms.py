# -*- coding: utf-8 -*-
#########################################################################
#
# Copyright (C) 2016 OSGeo
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
#
#########################################################################
from django import forms

class update_geoserver_params(forms.Form):
    workspace = forms.CharField(required=False, max_length=100, initial="", widget=forms.TextInput(attrs={'id': 'post-workspace'}))
    repository = forms.CharField(required=False, max_length=100, initial="", widget=forms.TextInput(attrs={'id': 'post-store'}))
    ignore_known = forms.BooleanField(required=False, initial=True, widget=forms.CheckboxInput(attrs={'id': 'post-ignore_known'}))
    remove_deleted = forms.BooleanField(required=False, initial=True, widget=forms.CheckboxInput(attrs={'id': 'post-remove_deleted'}))