var createMapThumbnail = function(obj_id) {
    var xmap = $('.ol-viewport');
    height = xmap.height();
    width = xmap.width();

    var url = window.location.pathname.replace('/view', '');

    if (typeof obj_id != 'undefined' && url.indexOf('new')) {
        url = url.replace('new', obj_id);
    }

    url += '/thumbnailOL3';
    miniature_data = {
        "attributes": {
            "map": {
                "dpi": 96,
                "layers": [],
                "rotation": 0
            }
        },
        "layout": "map miniature"
    };

    var layerreverse = [];
    miniature_data.attributes.map.bbox =  miniature_map.getView().calculateExtent(miniature_map.getSize());
    miniature_data.attributes.map.projection =  miniature_map.getView().getProjection().getCode();
    miniature_map.getLayers().forEach(function(layer) {
        layerreverse.push(layer);
    });
    layerreverse.reverse();
    layerreverse.forEach(function(layer) {
        miniature_data.attributes.map.layers.push(
            {
                'baseURL': layer.get('source').getUrls()[0],
                "customParams": {"TRANSPARENT": true},
                "imageFormat": "image/png",
                "layers": [layer.get('source').getParams().LAYERS],
                "opacity": 1,
                "type": "WMS"
            }
        );
    });

    $.ajax({
        type: "POST",
        url: url,
        data: JSON.stringify(miniature_data),
        success: function (data, status, jqXHR) {
            console.info(data, status, jqXHR);
            return true;
        },
        error: function (xhr) {
            console.info(xhr.responseText);
        }
    });
};