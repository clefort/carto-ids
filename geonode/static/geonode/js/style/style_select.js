var gs_layer;

// Send fresh request on every reload
$.ajaxSetup({ cache: false });

// get Layer JSON from Geoserver REST
$.ajax({
    url: "/geoserver/rest/layers/" + layerName + ".json",
    type: "GET",
    async: false,
    username: gs_credentials['username'],
    password: gs_credentials['password'],

    success: function (responseText) {
        var jsondata = eval(responseText);
        gs_layer = jsondata.layer;
        console.log("The request successfully retrieved the layer JSON from Geoserver");
    },
    error: function (responseText) {
        console.log("The request couldn't recover the layer JSON from Geoserver");
    }
});

// function executed when radio button clicked
function selectStyle(a){
    // get style name from input's id
    var selectedStyle = a.id;

    // change layers default style in the retrieved layer
    gs_layer.defaultStyle.name = selectedStyle;

    // putting back the Layer JSON with the new default style
    $.ajax({
        url: "/geoserver/rest/layers/"+layerName+".json",
        type: "PUT",
        async: true,
        username: gs_credentials['username'],
        password: gs_credentials['password'],
        headers: {
            "Content-type": "application/json"
        },
        data: '{"layer":' + JSON.stringify(gs_layer) + '}',

        success: function(responseText) {
            console.log("The request sent the layer JSON to Geoserver");
        },
        error: function(responseText) {
            console.log("The request couldn't send the layer JSON to Geoserver");
        }
    });

    // constructing the new url with GET params
    $("#styleSelectForm").attr("action", layerDetailUrl+"?style="+selectedStyle);

    // submit form
    $("#styleSelectForm").submit();
}