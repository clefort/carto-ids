        $(function() {
            // This function gets cookie with a given name
            function getCookie(name) {
                var cookieValue = null;
                if (document.cookie && document.cookie != '') {
                    var cookies = document.cookie.split(';');
                    for (var i = 0; i < cookies.length; i++) {
                        var cookie = jQuery.trim(cookies[i]);
                        // Does this cookie string begin with the name we want?
                        if (cookie.substring(0, name.length + 1) == (name + '=')) {
                            cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                            break;
                        }
                    }
                }
                return cookieValue;
            }
            var csrftoken = getCookie('csrftoken');

            /*
            The functions below will create a header with csrftoken
            */

            function csrfSafeMethod(method) {
                // these HTTP methods do not require CSRF protection
                return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));
            }
            function sameOrigin(url) {
                // test that a given url is a same-origin URL
                // url could be relative or scheme relative or absolute
                var host = document.location.host; // host + port
                var protocol = document.location.protocol;
                var sr_origin = '//' + host;
                var origin = protocol + sr_origin;
                // Allow absolute or scheme relative URLs to same origin
                return (url == origin || url.slice(0, origin.length + 1) == origin + '/') ||
                    (url == sr_origin || url.slice(0, sr_origin.length + 1) == sr_origin + '/') ||
                    // or any other URL that isn't scheme relative or absolute i.e relative.
                    !(/^(\/\/|http:|https:).*/.test(url));
            }

            $.ajaxSetup({
                beforeSend: function(xhr, settings) {
                    if (!csrfSafeMethod(settings.type) && sameOrigin(settings.url)) {
                        // Send the token to same-origin, relative URLs only.
                        // Send the token only if the method warrants CSRF protection
                        // Using the CSRFToken value acquired earlier
                        xhr.setRequestHeader("X-CSRFToken", csrftoken);
                    }
                },
                cache: false
            });

        });
        // this event trigger is not supported in IE11
        //$('#post-form').on('submit', function(event){
        $('#start_synchro').click(function(event){
            event.preventDefault();
            request_update_log();
        });

        function request_update_log() {
            $('#start_synchro').prop('disabled', true);
            $('#output_log p').remove();
            $('#output_log').append('<p>Operation in progress, Please wait ...</p>');
            $.ajax({
                url : "update_logs/", // the endpoint
                type : "POST", // http method
                data : {
                    the_workspace : $('#post-workspace').val(),
                    the_store : $('#post-store').val(),
                    the_ignore_known : $('#post-ignore_known').is(':checked'),
                    the_remove_deleted : $('#post-remove_deleted').is(':checked'),
                    the_author: $('#author_search_input').val()
                }, // data sent with the post request

                // handle a successful response
                success : function(json) {
                    $('#start_synchro').prop('disabled', false);
                    $('#output_log p').append('<p>Done !</p>');
                    for ( var i = 0; i < json.length; i++ ) {
                        $('#output_log').append('<p>'+json[i]+'</p>');
                    }
                },
                // handle a non-successful response
                error : function(xhr,errmsg,err) {
                    $('#start_synchro').prop('disabled', false);
                    $('#output_log').append('<p>We have encountered an error: '+errmsg+'</p>');
                }
            });
        };

        $("#clear_msg").click(function(){
            $('#output_log p').remove();
        });