Cart'O

Dans le cadre du projet P0026b relatif à la refonte du SIG de l’Agence, il a été décidé de mettre en œuvre une infrastructure de données spatiales (IDS) qui s’appuie sur la solution libre et open source GeoNode (geonode.org). Différents développements ont été réalisés sur des composants de cette IDS afin de corriger des dysfonctionnements et de s’adapter aux attentes de l’Agence. Toutefois, certaines de ces contributions apportent des améliorations ou corrections qui peuvent être bénéfiques à l’ensemble de la communauté GeoNode, c’est pourquoi il a été décidé de les reverser à cette dernière en licence ouverte.

Ces contributions (corrections de bugs et enrichissement des traductions françaises) sont portées par l’Agence de l’eau Loire-Bretagne (http://www.eau-loire-bretagne.fr) dans le cadre de la mise en œuvre de son SIG s’articulant autour de GeoNode.
Les développements informatiques ont été réalisés pour l’Agence de l’eau Loire-Bretagne par l’entreprise de services du numérique Capgemini (http://www.fr.capgemini.com/).
L’Agence de l’eau, établissement public français, s’engage depuis plus de 50 ans aux côtés des élus et des usagers de l’eau pour la qualité de l’eau et des milieux aquatiques.

Cart'O est distribué en licence GPL V3.
La version 2.5.4 de geoNode a été utilisée

GeoNode License
===============

GeoNode is Copyright 2016 Open Source Geospatial Foundation (OSGeo).

GeoNode is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

GeoNode is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with GeoNode.  If not, see <http://www.gnu.org/licenses/>.

	